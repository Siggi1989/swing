cd `dirname $0`

export CLASSPATH=$CLASSPATH:./../../JPlanner.jar:mysql-connector-java-8.0.17.jar:
mkdir ./bin
javac -d ./bin ./src/*.java
cd bin
mkdir ./File
cp ../File/*.* ./File/
java -cp ./:./../../../JPlanner.jar:mysql-connector-java-8.0.17.jar MainWindow
cd ..
echo “Done”