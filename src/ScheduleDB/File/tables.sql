DROP TABLE IF EXISTS `Schedules`
CREATE TABLE `Schedules` (`id` int(11) NOT NULL, `name` text NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8
DROP TABLE IF EXISTS `Appointments`
CREATE TABLE `Appointments`( `id` text NOT NULL, `scheduleId` int(11) NOT NULL, `startTime` text NOT NULL, `endTime` text NOT NULL, `headerText` text NOT NULL, `detailsText` text NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8
DROP TABLE IF EXISTS`Contacts`
CREATE TABLE `Contacts` (`id` text NOT NULL, `scheduleId` int(11) NOT NULL, `firstName` text NOT NULL, `lastName` text NOT NULL, `phoneNumber` text, `emailAddress` text ) ENGINE=InnoDB DEFAULT CHARSET=utf8
DROP TABLE IF EXISTS `Schedule_Contacts`
CREATE TABLE `Schedule_Contacts` (`scheduleId` int(11) NOT NULL, `contactId` text NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8
DROP TABLE IF EXISTS `Appointments_Contacts`
CREATE TABLE `Appointments_Contacts` (`scheduleId` int(11) NOT NULL, `appId` text NOT NULL, `contactId` text NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8