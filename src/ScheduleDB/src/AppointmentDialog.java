import com.mindfusion.common.DateTime;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.CalendarAdapter;
import com.mindfusion.scheduling.DateChangedEvent;
import com.mindfusion.scheduling.ResourceDateEvent;
import com.mindfusion.scheduling.model.Appointment;
import com.mindfusion.scheduling.model.Contact;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.TimeZone;

public class AppointmentDialog extends BaseDialog {

    private static final long serialVersionUID = 1L;

    private final TimeZone TIMEZONE = TimeZone.getDefault();

    private DateComboBox _startTime;
    private JSpinner _startTimeSpinner;

    private DateComboBox _endTime;
    private JSpinner _endTimeSpinner;

    private JTextField _headerText;
    private JTextArea _detailsText;

    private CheckedListBox _contactBox;

    private Appointment _appointment;

    private Map<Integer, Contact> _contactMap;


    AppointmentDialog(String title, Window parent, Calendar schedule, Appointment app) {
        super(parent, ModalityType.APPLICATION_MODAL);
        this._appointment = app;
        setLayout(null);
        setTitle(title);
        setType(Type.UTILITY);
        setMaximumSize(new Dimension(600,400));
        setMinimumSize(new Dimension(600,400));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);


        JLabel st_label = new JLabel("Start Time :");
        st_label.setBounds(5, 20, 100, 30);

        _startTime = new DateComboBox();
        if (app != null){
            _startTime.setValue(app.getStartTime());
            _startTime.setSelectedItem(convertToJavaDate(app.getStartTime()));
        }
        _startTime.setBounds(85, 25, 100, 20);
        _startTime.getCalendar().addCalendarListener(new CalendarAdapter(){

            @Override
            public void visibleDateChanged(DateChangedEvent dateChangedEvent) {
                super.visibleDateChanged(dateChangedEvent);
                _startTime.getCalendar().setDate(dateChangedEvent.getNewDate());
            }

            @Override
            public void dateClick(ResourceDateEvent resourceDateEvent) {
                super.dateClick(resourceDateEvent);
                _startTime.getCalendar().setDate(resourceDateEvent.getDate());

            }
        });

        SpinnerDateModel _startTimeModel = new SpinnerDateModel(convertToJavaDate(_startTime.getCalendar().getDate()),
                null, null, java.util.Calendar.MINUTE);
        _startTimeSpinner = new JSpinner();
        _startTimeSpinner.setModel(_startTimeModel);
        JSpinner.DateEditor startEditor = new JSpinner.DateEditor(_startTimeSpinner, "hh:mm a");
        startEditor.getTextField().setEditable(false);
        _startTimeSpinner.setEditor(startEditor);
        _startTimeSpinner.setValue(convertToJavaDate(_startTime.getCalendar().getDate()));
        _startTimeSpinner.setBounds(195, 25, 80,20);

        JLabel en_label = new JLabel("End Time :");
        en_label.setBounds(300, 20, 100, 30);

        _endTime = new DateComboBox();
        if (app != null){
            _endTime.setValue(app.getEndTime());
            _endTime.setSelectedItem(convertToJavaDate(app.getEndTime()));
        }
        _endTime.setBounds(375, 25, 100, 20);
        _endTime.getCalendar().addCalendarListener(new CalendarAdapter(){
            @Override
            public void visibleDateChanged(DateChangedEvent dateChangedEvent) {
                super.visibleDateChanged(dateChangedEvent);
                _endTime.getCalendar().setDate(dateChangedEvent.getNewDate());
            }

            @Override
            public void dateClick(ResourceDateEvent resourceDateEvent) {
                super.dateClick(resourceDateEvent);
                _endTime.getCalendar().setDate(resourceDateEvent.getDate());

            }
        });

        SpinnerDateModel _endTimeModel = new SpinnerDateModel(convertToJavaDate(_endTime.getCalendar().getDate()),
                null, null, java.util.Calendar.MINUTE);
        _endTimeSpinner = new JSpinner();
        _endTimeSpinner.setModel(_endTimeModel);
        JSpinner.DateEditor endEditor = new JSpinner.DateEditor(_endTimeSpinner, "hh:mm a");
        endEditor.getTextField().setEditable(false);
        _endTimeSpinner.setEditor(endEditor);
        _endTimeSpinner.setValue(convertToJavaDate(app == null
                ?_endTime.getCalendar().getDate().addHours(6)
                : _endTime.getCalendar().getDate()));
        _endTimeSpinner.setBounds(485, 25, 80, 20);

        JLabel headerLabel = new JLabel("Header text :");
        headerLabel.setBounds(5, 50, 100, 30);

        _headerText = new JTextField();
        _headerText.setBounds(80, 57, 300, 20);
        if (app != null)
            _headerText.setText(app.getHeaderText());

        JLabel details_label = new JLabel("Details : ");
        details_label.setBounds(5, 80, 100, 30);

        _detailsText = new JTextArea();
        if (app != null){
            _detailsText.setText(app.getDetails());
        }
        _detailsText.setLineWrap(true);
        _detailsText.setWrapStyleWord(true);
        _detailsText.setBorder(BorderFactory.createMatteBorder(1,1,1,1, Color.LIGHT_GRAY));
        _detailsText.setBounds(80, 90, 300, 220);

        _contactBox = new CheckedListBox();
        _contactBox.setBorder(BorderFactory.createMatteBorder(1,1,1,1, Color.LIGHT_GRAY));

        JScrollPane _contactBoxPane = new JScrollPane(_contactBox,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        _contactBoxPane.setBounds(390, 90, 175, 220);

        _contactMap = new Hashtable<>();
        for (Contact contact : schedule.getContacts()){
            String fullName = contact.getFirstName() + " " + contact.getLastName();
            int index = _contactBox.addItem(fullName);
            _contactMap.put(index, contact);
            if (app != null && app.getContacts().contains(contact))
                _contactBox.setItemChecked(index, true, true);
        }

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                TimeZone.setDefault(TIMEZONE);
            }
        });

        if (app != null)
            _okBtn.setText("Update");

        _okBtn.setLocation(190, 320);
        _cancelBtn.setLocation(300, 320);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                _headerText.requestFocus();
            }
        });

        getContentPane().add(st_label);
        getContentPane().add(_startTime);
        getContentPane().add(_startTimeSpinner);
        getContentPane().add(en_label);
        getContentPane().add(_endTime);
        getContentPane().add(_endTimeSpinner);
        getContentPane().add(headerLabel);
        getContentPane().add(_headerText);
        getContentPane().add(details_label);
        getContentPane().add(_detailsText);
        getContentPane().add(_contactBoxPane);
        getContentPane().add(_okBtn);
        getContentPane().add(_cancelBtn);
        setLocationRelativeTo(parent);
        setVisible(true);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if ("OK".equals(e.getActionCommand())) {

            if (_contactBox.getModel().getSize() == 0 || !contactIsSelected()){
                dispose();
                return;
            }
            createAppointment();
            for (int i = 0; i < _contactBox.getModel().getSize(); i++) {
                if (_contactBox.getChekedItem(i)) {
                    Contact contact = _contactMap.get(i);
                    if (!_appointment.getContacts().contains(contact)) {
                        _appointment.getContacts().add(contact);
                    }
                }else {
                    if (_contactMap.get(i) != null  && _appointment.getContacts().contains(_contactMap.get(i))){
                        _appointment.getContacts().remove(_contactMap.get(i));
                    }
                }
            }
            result = DialogResult.OK;
            dispose();
            TimeZone.setDefault(TIMEZONE);
        } else {
            result = DialogResult.Cancel;
            dispose();
            TimeZone.setDefault(TIMEZONE);
        }
    }


    private boolean contactIsSelected(){
        for (int i = 0; i< _contactBox.getModel().getSize(); i++){
            if (_contactBox.getChekedItem(i))
                return true;
        }
        return false;
    }

    private Date convertToJavaDate(DateTime dateTime){
        TimeZone.setDefault(dateTime.toJavaCalendar().getTimeZone());
        java.text.DateFormat readFormat = new java.text.SimpleDateFormat( "MM/d/yy H:mm:ss a z");
        readFormat.setTimeZone(dateTime.toJavaCalendar().getTimeZone());
        Date date = null;
        try {
            String s = readFormat.format(dateTime.toJavaCalendar().getTime());
            date = readFormat.parse(s);
        } catch ( ParseException e ) {
            e.printStackTrace();
        }
        return date;
    }

    private DateTime getStartTime(){

        Date start =  (Date)_startTimeSpinner.getValue();
        DateTime startT = new DateTime(start);
        DateTime startDate = _startTime.getCalendar().getDate();

        return new DateTime(startDate.getYear(), startDate.getMonth(),
                startDate.getDay(), startT.getHour(), startT.getMinute(), 0);
    }

    private DateTime getEndTime(){

        Date end =  (Date)_endTimeSpinner.getValue();
        DateTime endT = new DateTime(end);
        DateTime endDate = _endTime.getCalendar().getDate();

        return new DateTime(endDate.getYear(), endDate.getMonth(), endDate.getDay(),
                endT.getHour(), endT.getMinute(), 0);
    }

    private  void createAppointment(){

        if (_appointment == null)
            _appointment = new Appointment();

        DateTime startTime = getStartTime();
        DateTime endTime = getEndTime();

        if (startTime.compareTo(endTime) > 0)
            startTime = endTime;

        _appointment.setStartTime(startTime);
        _appointment.setEndTime(endTime);

        String h_text = _headerText.getText();
        String d_text = _detailsText.getText();

        _appointment.setHeaderText(h_text);
        _appointment.setDetails(d_text);

    }

    Appointment getAppointment() {
        return this._appointment;
    }
}
