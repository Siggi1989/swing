import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Objects;

class Credentials {

    static String path;
    static String user;
    static String password;

    private static Credentials instance = null;

    private Credentials() {
    }

    static Credentials getInstance() {

        if (instance == null)
            instance = new Credentials();
        return instance;
    }

    boolean exists() {
        boolean b = false;
        Path filePath = null;
        try {
            filePath = Paths.get(getClass().getResource("File/").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (Files.exists(Objects.requireNonNull(filePath))){
            try {
                FileVisitor visitor = new FileVisitor();
                Files.walkFileTree(filePath, visitor);
                b = visitor.isFilePresent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return b;
    }

    void assignCredentials() {
        try {
            Path filePath = Paths.get(getClass().getResource("File/credentials.txt").toURI());

            List<String> content = Files.readAllLines(filePath, StandardCharsets.UTF_8);

            for(int i=0; i<content.size(); i++) {
                if(i==0)
                    path = content.get(i).substring(7);
                else if(i==1)
                    user = content.get(i).substring(11);
                else if(i==2)
                    password = content.get(i).substring(11);
            }

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    class FileVisitor extends SimpleFileVisitor<Path>{

        private boolean filePresent = false;

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            if ("credentials.txt".equals(file.getFileName().toString())){
                filePresent = true;
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        boolean isFilePresent() {
            return filePresent;
        }
    }
}
