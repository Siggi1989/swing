import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class ConnectionWindow extends BaseDialog {


    private static final long serialVersionUID = 1L;
    private JTextField _hostField;
    private JTextField _database_name;
    private JTextField _userName;
    private JPasswordField _password;

    ConnectionWindow(Window parent) {
        super(parent, ModalityType.APPLICATION_MODAL);
        parent.setVisible(false);
        setLayout(null);
        setTitle("Connect to database");
        setType(Type.UTILITY);
        setMaximumSize(new Dimension(480, 350));
        setMinimumSize(new Dimension(480, 350));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);


        JLabel hostExampleLabel = new JLabel();
        hostExampleLabel.setFont(new Font(hostExampleLabel.getFont().getName(), Font.BOLD | Font.ITALIC, 13));
        hostExampleLabel.setForeground(Color.DARK_GRAY);
        hostExampleLabel.setBounds(40, 5, 480, 30);
        hostExampleLabel.setText("MySQL instance name (without jdbc prefix):");

        _hostField = new JTextField();
        _hostField.setBounds(40, 30, 400, 25);
		_hostField.setText("localhost");

        JLabel databaseNameLabel = new JLabel();
        databaseNameLabel.setFont(new Font(databaseNameLabel.getFont().getName(), Font.BOLD | Font.ITALIC, 13));
        databaseNameLabel.setForeground(Color.DARK_GRAY);
        databaseNameLabel.setBounds(40, 60, 480, 30);
        databaseNameLabel.setText("Database name (created automatically if it does not exist):");

        _database_name = new JTextField();
        _database_name.setBounds(40, 85, 400, 25);
		_database_name.setText("schedule_db");

        JLabel userNameLabel = new JLabel("Database user:");
        userNameLabel.setFont(new Font(userNameLabel.getFont().getName(), Font.ITALIC | Font.BOLD, 13));
        userNameLabel.setForeground(Color.DARK_GRAY);
        userNameLabel.setBounds(40, 115, 480, 30);

        _userName = new JTextField();
        _userName.setBounds(40, 140, 400, 25);

        JLabel passLabel = new JLabel("Password:");
        passLabel.setFont(new Font(passLabel.getFont().getName(), Font.BOLD | Font.ITALIC, 13));
        passLabel.setBackground(Color.DARK_GRAY);
        passLabel.setBounds(40, 170, 480, 30);

        _password = new JPasswordField();
        _password.setBounds(40, 195, 400, 25);


        _okBtn.setLocation(120, 250);
        _cancelBtn.setLocation(240, 250);

        getContentPane().add(hostExampleLabel);
        getContentPane().add(_hostField);
        getContentPane().add(databaseNameLabel);
        getContentPane().add(_database_name);
        getContentPane().add(userNameLabel);
        getContentPane().add(_userName);
        getContentPane().add(passLabel);
        getContentPane().add(_password);
        getContentPane().add(_okBtn);
        getContentPane().add(_cancelBtn);
        setLocationRelativeTo(parent);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ("OK".equals(e.getActionCommand())) {
            String host = _hostField.getText().trim();
            String database = _database_name.getText().trim();
            String user = _userName.getText().trim();
            String pass = new String(_password.getPassword());
            DBConnection connection = new DBConnection(host, database, user, pass);
            if (connection.connect()) {
                connection.createTables();
                saveCredentials();
                canDisplay = true;
            } else {
                displayErroMsg(connection.getErrorMsg());
                canDisplay = false;
            }
            this.dispose();
        } else {
            this.dispose();
            canDisplay = false;
        }

    }


    private void displayErroMsg(String errorMsg) {

        JTextArea jTextArea = new JTextArea(errorMsg);
        jTextArea.setMaximumSize(new Dimension(200, 200));
        jTextArea.setMinimumSize(new Dimension(200, 200));
        JScrollPane scrollPane = new JScrollPane(jTextArea,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JOptionPane.showMessageDialog(this, scrollPane, "Error", JOptionPane.ERROR_MESSAGE);

    }

    private void saveCredentials() {

        String path = "PATH = " + Credentials.path;
        String user = "USERNAME = " + Credentials.user;
        String pass = "PASSWORD = " + Credentials.password;

        try {
            Path parent = Paths.get(getClass().getResource("File/").toURI());
            Path file = parent.resolve(Paths.get("credentials.txt"));
            if (!Files.exists(file))
                Files.createFile(file);
            Files.write(file, Arrays.asList(path, user, pass), StandardOpenOption.WRITE, StandardOpenOption.SYNC);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

    }

    void display() {
        this.setVisible(true);
    }

    boolean canDisplay;

}
