import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

abstract class BaseDialog extends JDialog implements ActionListener {

    JButton _okBtn;
    JButton _cancelBtn;
    DialogResult result;

    BaseDialog(Window parent, ModalityType applicationModal) {
        super(parent, applicationModal);
        result = DialogResult.Cancel;


        _okBtn = new JButton("Ok");
        _okBtn.setActionCommand("OK");
        _okBtn.addActionListener(this);
        _okBtn.setFocusPainted(false);
        _okBtn.setSize(new Dimension(90, 30));

        _cancelBtn = new JButton("Cancel");
        _cancelBtn.addActionListener(this);
        _cancelBtn.setFocusPainted(false);
        _cancelBtn.setSize(new Dimension(90, 30));

    }

    DialogResult getResult() {
        return result;
    }
}
