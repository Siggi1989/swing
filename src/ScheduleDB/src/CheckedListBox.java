import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Vector;

class CheckedListBox extends JList<JCheckBox> {
    private static final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);
    private final Vector<JCheckBox> jCheckBoxes;
    private HashMap<Integer, Boolean> selectionMap;

    CheckedListBox() {
        jCheckBoxes = new Vector<>();
        setListData(jCheckBoxes);
        selectionMap = new HashMap<>();
        setCellRenderer(new CellRenderer());

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int index = locationToIndex(e.getPoint());

                if (index != -1) {
                    JCheckBox checkbox = getModel().getElementAt(index);
                    checkbox.setSelected(!checkbox.isSelected());
                    selectionMap.put(index, checkbox.isSelected());
                    repaint();
                }
            }
        });

        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    boolean getChekedItem(int index) {

        return selectionMap.get(index) == null ? false : selectionMap.get(index);
    }

    void setItemChecked(int index, boolean checked){
        setItemChecked(index, checked, false);
    }

    void setItemChecked(int index, boolean checked, boolean fromLoad){
        if (!selectionMap.isEmpty() && selectionMap.containsKey(index))
            selectionMap.put(index, checked);
        if (index <= this.getModel().getSize())
            this.getModel().getElementAt(index).setSelected(checked);
        if (fromLoad && !selectionMap.containsKey(index))
            selectionMap.put(index, checked);
    }
    class CellRenderer implements ListCellRenderer<JCheckBox> {

        @Override
        public Component getListCellRendererComponent(
                JList<? extends JCheckBox> list,
                JCheckBox value,
                int index,
                boolean isSelected,
                boolean cellHasFocus)
        {
            value.setBackground(isSelected ? Color.BLUE : getBackground());
            value.setForeground(isSelected ? Color.WHITE : getForeground());
            value.setEnabled(isEnabled());
            value.setFont(getFont());
            value.setFocusPainted(false);
            value.setBorderPainted(true);
            value.setBorder(isSelected ? UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);
            return value;
        }
    }


    int addItem(String itemName) {
        JCheckBox jCheckBox = new JCheckBox(itemName);
        jCheckBox.setSelected(false);
        jCheckBoxes.add(jCheckBox);

        return jCheckBoxes.indexOf(jCheckBox);
    }
}