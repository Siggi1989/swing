import com.mindfusion.scheduling.model.Contact;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.regex.Pattern;

public class ContactDialog extends BaseDialog {

    private static final long serialVersionUID = 1L;
    //simple email validator validates empty string and email address with @ symbol only.
    private static final Pattern SIMPLE_EMAIL_PATTERN = Pattern.compile("^$|^(.+)@(.+)$");
    // simple phone number validator validate empty string and phone with (-, +, /, *) symbols
    private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^$|^(?=.*[0-9])[- +*/0-9]+$");
    private JTextField _fnField;
    private JTextField _lnField;
    private JTextField _emailAdressField;
    private JTextField _phoneNumberField;
    private Contact _contact;

    ContactDialog(Window parent){
        super(parent, Dialog.ModalityType.APPLICATION_MODAL);
        setLayout(null);
        setTitle("Add Contact");
        setType(Window.Type.UTILITY);
        setMaximumSize(new Dimension(350,250));
        setMinimumSize(new Dimension(350,250));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);



        JLabel fnLabel = new JLabel("First Name : ");
        fnLabel.setBounds(20,10,80,30);

        _fnField = new JTextField();
        _fnField.setName("FirstName");
        _fnField.setBounds(95, 16, 200, 20);

        JLabel lnLabel = new JLabel("Last Name : ");
        lnLabel.setBounds(20, 40, 80, 30);

        _lnField = new JTextField();
        _lnField.setName("LastName");
        _lnField.setBounds(95, 46, 200, 20);

        JLabel emailLabel = new JLabel("Email :", SwingConstants.LEFT);
        emailLabel.setBounds(20, 70, 80, 30);

        _emailAdressField = new JTextField();
        _emailAdressField.setName("Email");
        _emailAdressField.setBounds(95, 76, 200, 20);
		_emailAdressField.setText("test@test.test");

        JLabel phoneLabel = new JLabel("Phone :", SwingConstants.LEFT);
        phoneLabel.setBounds(20, 100, 80, 30);

        _phoneNumberField = new JTextField();
        _phoneNumberField.setName("Phone");
        _phoneNumberField.setBounds(95, 106, 200, 20);
		_phoneNumberField.setText("123123123");

        _okBtn.setLocation(70, 150);
        _cancelBtn.setLocation(180, 150);

        getContentPane().add(fnLabel);
        getContentPane().add(_fnField);
        getContentPane().add(lnLabel);
        getContentPane().add(_lnField);
        getContentPane().add(emailLabel);
        getContentPane().add(_emailAdressField);
        getContentPane().add(phoneLabel);
        getContentPane().add(_phoneNumberField);
        getContentPane().add(_okBtn);
        getContentPane().add(_cancelBtn);
        setLocationRelativeTo(parent);
        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if ("OK".equals(e.getActionCommand())) {
            String firstName = _fnField.getText();
            if (firstName == null || firstName.isEmpty()) {
                _fnField.setBorder(
                        BorderFactory.createLineBorder(Color.RED));
                return;
            } else {
                _fnField.setBorder(
                        BorderFactory.createLineBorder(Color.DARK_GRAY));
            }
            String lastName = _lnField.getText();
            if (lastName == null || lastName.isEmpty()) {
                _lnField.setBorder(BorderFactory.createLineBorder(Color.RED));
                return;
            } else {
                _lnField.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            }
            if (!SIMPLE_EMAIL_PATTERN.matcher(_emailAdressField.getText()).matches()) {
                _emailAdressField.setBorder(BorderFactory.createLineBorder(Color.RED));
                return;
            } else {
                _emailAdressField.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            }
            if (!PHONE_NUMBER_PATTERN.matcher(_phoneNumberField.getText()).matches()) {
                _phoneNumberField.setBorder(BorderFactory.createLineBorder(Color.RED));
                return;
            } else {
                _phoneNumberField.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            }

            result = DialogResult.OK;
            _contact = new Contact();
            _contact.setFirstName(firstName);
            _contact.setLastName(lastName);
            _contact.setEmail(_emailAdressField.getText());
            _contact.setPhone(_phoneNumberField.getText());
            dispose();
        } else {
            result = DialogResult.Cancel;
            dispose();
        }
    }

    Contact getContact(){
        return this._contact;
    }

}
