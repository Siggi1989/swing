import com.mindfusion.common.DateTime;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.CalendarAdapter;
import com.mindfusion.scheduling.DateChangedEvent;
import com.mindfusion.scheduling.ResourceDateEvent;
import com.sun.java.swing.plaf.motif.MotifComboBoxUI;
import com.sun.java.swing.plaf.windows.WindowsComboBoxUI;

import javax.swing.*;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

class DateComboBox extends JComboBox {

    private SimpleDateFormat dateFormat;
    private Calendar calendar;
    private static final long serialVersionUID = 1L;

    @Override
    public void setSelectedItem(Object anObject) {
        removeAllItems();
        if (anObject instanceof Date){
            Date f = (Date) anObject;
            anObject = dateFormat.format(f);
        }
        addItem(anObject);
        super.setSelectedItem(anObject);
    }

    @Override
    public void updateUI() {
        ComboBoxUI cui = (ComboBoxUI) UIManager.getUI(this);
        if (cui instanceof MotifComboBoxUI) {
            cui = new DateComboBox.MotifDateComboBoxUI();
        } else if (cui instanceof WindowsComboBoxUI) {
            cui = new WindowsDateComboBoxUI();
        }else{
            cui = new MetalDateComboBoxUI();
        }
        setUI(cui);
    }

    class MetalDateComboBoxUI extends MetalComboBoxUI {
        @Override
        protected ComboPopup createPopup() {
            return new DateComboBox.DatePopup( comboBox );
        }
    }

    class WindowsDateComboBoxUI extends WindowsComboBoxUI {
        @Override
        protected ComboPopup createPopup() {
            return new DateComboBox.DatePopup( comboBox );
        }
    }

    class MotifDateComboBoxUI extends MotifComboBoxUI {
        @Override
        protected ComboPopup createPopup() {
            return new DateComboBox.DatePopup( comboBox );
        }
    }


    class DatePopup extends MouseAdapter implements ComboPopup {

        JComboBox comboBox;
        JPopupMenu popup;

        DatePopup(JComboBox comboBox) {
            this.comboBox = comboBox;
            dateFormat = new SimpleDateFormat("M/d/yyyy");
            calendar = new Calendar();
            calendar.getSelection().setAllowMultiple(false);
            calendar.addCalendarListener(new CalendarChangesListener());
            Date date = calendar.getDate().toJavaCalendar().getTime();
            String d = dateFormat.format(date);
            this.comboBox.setSelectedItem(d);
            this.comboBox.setBackground(Color.WHITE);
            this.comboBox.setRenderer(new CellRenderer());
            initializePopup();
        }

        @Override
        public void show() {
            try {
                String g = Objects.requireNonNull(comboBox.getSelectedItem()).toString();
                Date date = dateFormat.parse(g);
                calendar.setDate(new DateTime(date));
            } catch (Exception e) {
                e.printStackTrace();
            }
            updatePopup();
            popup.show(comboBox, 0, comboBox.getHeight());
        }

        @Override
        public void hide() {
            popup.setVisible(false);
        }

        JList list = new JList();
        @Override
        public JList getList() {
            return list;
        }

        @Override
        public MouseListener getMouseListener() {
            return this;
        }

        @Override
        public MouseMotionListener getMouseMotionListener() {
            return this;
        }

        @Override
        public KeyListener getKeyListener() {
            return null;
        }

        @Override
        public boolean isVisible() {
            return popup.isVisible();
        }

        @Override
        public void uninstallingUI() { }


        @Override
        public void mouseClicked(MouseEvent e) {
            if ( !SwingUtilities.isLeftMouseButton(e) )
                return;
            if ( !comboBox.isEnabled() )
                return;
            if ( comboBox.isEditable() ) {
                comboBox.getEditor().getEditorComponent().requestFocus();
            } else {
                comboBox.requestFocus();
            }
            togglePopup();
        }

        void togglePopup() {
            if (isVisible())
                hide();
            else
                show();
        }
        private void initializePopup() {
            popup = new JPopupMenu();
            popup.setBorder(BorderFactory.createLineBorder(Color.black));
            popup.add(BorderLayout.CENTER, calendar);
        }

        void updatePopup() {
            popup.add(BorderLayout.CENTER, calendar);
            popup.pack();
        }

        class CalendarChangesListener extends CalendarAdapter {

            @Override
            public void visibleDateChanged(DateChangedEvent dateChangedEvent) {
                super.visibleDateChanged(dateChangedEvent);
                setSelectedItem(dateFormat.format(dateChangedEvent.getNewDate().toJavaCalendar().getTime()));
            }

            @Override
            public void dateClick(ResourceDateEvent resourceDateEvent) {
                super.dateClick(resourceDateEvent);
                setSelectedItem(dateFormat.format(resourceDateEvent.getDate().toJavaCalendar().getTime()));
                popup.setVisible(false);
            }



        }
        class CellRenderer extends DefaultListCellRenderer{
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (isSelected)
                    list.setSelectionBackground(Color.WHITE);
                return c;
            }
        }
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setValue(DateTime dateTime){
        calendar.setDate(dateTime);
    }

}
