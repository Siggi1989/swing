import com.mindfusion.scheduling.*;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.model.Appointment;
import com.mindfusion.scheduling.model.ItemEvent;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.*;

public class MainWindow extends JFrame implements ActionListener {

	public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            MainWindow mainWindow = new MainWindow();
            if (show)
                mainWindow.setVisible(true);
            else
                mainWindow.dispose();
        });
    }

    private MainWindow(){

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(700,500));
        setTitle("MindFusion.Scheduling Sample: MySQL Storage");

        Credentials credentials = Credentials.getInstance();

        if (credentials.exists()){
            credentials.assignCredentials();
            show = true;
        } else {
            ConnectionWindow connectionWindow = new ConnectionWindow(this);
            connectionWindow.display();
            show = connectionWindow.canDisplay;
        }
        
        if(show) 
        	DBConnection.config();

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");

        JMenuItem newItem = new JMenuItem("New");
        newItem.setActionCommand("new");
        newItem.addActionListener(this);

        JMenuItem saveItem = new JMenuItem("Save");
        saveItem.setActionCommand("save");
        saveItem.addActionListener(this);

        JMenuItem loadItem = new JMenuItem("Load");
        loadItem.setActionCommand("load");
        loadItem.addActionListener(this);

        menu.add(newItem);
        menu.add(saveItem);
        menu.add(loadItem);


        JMenu controlsMenu = new JMenu("Records");

        JMenu addItem = new JMenu("Insert");

        addContactsItem = new JMenuItem("Contact");
        addContactsItem.setEnabled(calendar != null);
        addContactsItem.addActionListener(this);
        addContactsItem.setActionCommand("insert_contact");

        addAppointmentItem = new JMenuItem("Appointment");
        addAppointmentItem.setEnabled(calendar != null);
        addAppointmentItem.addActionListener(this);
        addAppointmentItem.setActionCommand("insert_app");

        addItem.add(addContactsItem);
        addItem.add(addAppointmentItem);
        controlsMenu.add(addItem);

        controlsMenu.add(addItem);

        menuBar.add(menu);
        menuBar.add(controlsMenu);


        controlsPopupMenu = new JPopupMenu();
        controlsPopupMenu.setLightWeightPopupEnabled(true);

        updateApp = new JMenuItem("Update");
        updateApp.setActionCommand("update_app");
        updateApp.addActionListener(this);

        deleteApp = new JMenuItem("Delete");
        deleteApp.setActionCommand("delete_app");
        deleteApp.addActionListener(this);

        controlsPopupMenu.add(updateApp);
        controlsPopupMenu.add(deleteApp);


        JTextPane pane = new JTextPane();
        pane.setBackground(new Color(255,255, 225));
        pane.setSize(700,200);
        pane.setBorder(new LineBorder(Color.orange, 1));
        pane.setEditorKit(new HTMLEditorKit());
        pane.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(pane);

        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());



        SpringLayout layout = new SpringLayout();
        layout.putConstraint(SpringLayout.WEST, scrollPane, 0, SpringLayout.WEST, getContentPane());
        layout.putConstraint(SpringLayout.NORTH, scrollPane, 0, SpringLayout.NORTH, getContentPane());
        layout.putConstraint(SpringLayout.EAST, scrollPane, 0, SpringLayout.EAST, getContentPane());
        layout.putConstraint(SpringLayout.SOUTH, scrollPane, 70, SpringLayout.NORTH, getContentPane());

        layout.putConstraint(SpringLayout.WEST, contentPanel, 0, SpringLayout.WEST, getContentPane());
        layout.putConstraint(SpringLayout.NORTH, contentPanel, 70, SpringLayout.NORTH, getContentPane());
        layout.putConstraint(SpringLayout.EAST, contentPanel, 0, SpringLayout.EAST, getContentPane());
        layout.putConstraint(SpringLayout.SOUTH, contentPanel, 0, SpringLayout.SOUTH, getContentPane());

        setJMenuBar(menuBar);
        setLayout(layout);
        setInfo(pane);
        getContentPane().add(scrollPane);
        getContentPane().add(contentPanel);

    }

    private void setInfo(JTextPane pane) {

        pane.setText("<div style=\"padding: 2pt; font-family: Verdana; font-size: 11pt;\">"
                + "This sample demonstrates how a schedule could be stored in a "
                + "<strong>MySql</strong> database using <strong>JDBC</strong> connector. "
				+ "Create a new schedule using the File -> New command, and add contact rows "
				+ "from Records menu. Add appointments either by selecting a time range and typing "
				+ "or from the Records -> Insert menu."
                + "</div>");
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                if (canSave())
                    onSaveSchedule();
                break;
            case "load":
                onLoadSchedule();
                break;
            case "new":
                String name = JOptionPane.showInputDialog(
                        this,
                        "Enter Name",
                        "Select Schedule Name", JOptionPane.PLAIN_MESSAGE);
                if (name == null || name.isEmpty())
                    return;
                Crud.getSchedules();
                if (Crud.schedulesValueMap.containsKey(name)){
                    JOptionPane.showMessageDialog(this,
                            "Schedule with same name already exists in database",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                onCreateNewSchedule(name);
                break;
            case "insert_contact":
                ContactDialog contactDialog = new ContactDialog(this);
                if (contactDialog.getResult() == DialogResult.OK){
                    calendar.getSchedule().getContacts().add(contactDialog.getContact());
                    calendar.getContacts().add(contactDialog.getContact());
                }
                break;
            case "insert_app":
                AppointmentDialog appointmentDialog = new AppointmentDialog("Add Appointment",this, calendar, null);
                if (appointmentDialog.getResult() == DialogResult.OK)
                    calendar.getSchedule().getItems().add(appointmentDialog.getAppointment());
                break;
            case "update_app":
                appointmentDialog = new AppointmentDialog("Update Appointment", this, calendar, clickedApp);
                if (appointmentDialog.getResult() == DialogResult.OK){
                    if (clickedApp.getTag() != null)
                        Crud.updatedAppMap.put(clickedApp.getId(), clickedApp);
                }
                break;
            case "delete_app":
                if (clickedApp != null){
                    calendar.getSchedule().getItems().remove(clickedApp);
                    if (clickedApp.getTag() != null){
                        Crud.deletedAppMap.put(clickedApp.getId(), clickedApp);
                        Crud.updatedAppMap.remove(clickedApp.getId());
                    }
                }
                break;
        }
    }


    private void onSaveSchedule(){
        Crud.save(calendar);
    }

    private  void onLoadSchedule(){
        String [] data = Crud.getSchedules().toArray(new String[0]);
        if (data.length == 0)
            return;
        JList<String> sData = new JList<>(data);
        showSelectionList(sData);
    }

    private void onCreateNewSchedule(String name){

        contentPanel.removeAll();
        Crud.savedContactsMap.clear();
        Crud.savedAppMap.clear();
        Crud.updatedAppMap.clear();
        Crud.deletedAppMap.clear();

        calendar = new Calendar();
        calendar.setName(name);
        calendar.setCurrentView(CalendarView.ResourceView);
        calendar.setGroupType(GroupType.GroupByContacts);
        calendar.getResourceViewSettings().setTimelineScale(600);
        calendar.setTheme(ThemeType.Vista);
        calendar.addCalendarListener(new AppointmentClickListener());
        contentPanel.add(calendar, BorderLayout.CENTER, 0);

        addContactsItem.setEnabled(calendar != null);
        addAppointmentItem.setEnabled(calendar != null);

        contentPanel.repaint();
        contentPanel.revalidate();
        revalidate();
        repaint();
    }

    private void showSelectionList(JList<String> sData){

        JDialog dialog = new JDialog();
        dialog.setSize(200,300);
        dialog.setResizable(false);
        dialog.setAlwaysOnTop(true);

        JScrollPane scrollPane = new JScrollPane(sData,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        dialog.getContentPane().add(scrollPane);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        sData.addListSelectionListener(e -> {
            if (e.getValueIsAdjusting()){
                onCreateNewSchedule(sData.getSelectedValue());
                Crud.load(sData.getSelectedIndex()+1, calendar);
                calendar.revalidate();
                dialog.dispose();
            }

        });
    }

    private boolean canSave(){
        return calendar != null && calendar.getContacts().size() > 0;
    }

    private class AppointmentClickListener extends CalendarAdapter{

        @Override
        public void itemClick(ItemMouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3){

                Rectangle rectangle = calendar.getItemBounds(e.getItem());
                controlsPopupMenu.show(calendar, rectangle.x + e.getX(), rectangle.y + e.getY());
                clickedApp = (Appointment) e.getItem();
            }
        }


        @Override
        public void itemDeleted(ItemEvent itemEvent) {
            if (itemEvent.getItem() instanceof Appointment && itemEvent.getItem().getTag() != null){
                Crud.deletedAppMap.put(itemEvent.getItem().getId(), (Appointment) itemEvent.getItem());
                Crud.updatedAppMap.remove(itemEvent.getItem().getId());
            }
        }

        @Override
        public void itemInplaceEdited(ItemEvent itemEvent) {
            if (itemEvent.getItem() instanceof Appointment){
            	if (itemEvent.getItem().getTag() != null){
                    Crud.updatedAppMap.put(itemEvent.getItem().getId(),
                            (Appointment) itemEvent.getItem());
                }
            }
        }

        @Override
        public void itemModified(ItemModifiedEvent itemModifiedEvent) {
            if (itemModifiedEvent.getItem() instanceof Appointment){
            	if (itemModifiedEvent.getItem().getTag() != null){
                    Crud.updatedAppMap.put(itemModifiedEvent.getItem().getId(),
                            (Appointment) itemModifiedEvent.getItem());
                }
            }
        }
    }

    private static boolean show;
    private Calendar calendar;
    private Appointment clickedApp;
    private JPanel contentPanel;
    private JMenuItem addContactsItem;
    private JMenuItem addAppointmentItem;
    private JMenuItem updateApp, deleteApp;
    private JPopupMenu controlsPopupMenu;
	private static final long serialVersionUID = 3720977399096644388L;

}
