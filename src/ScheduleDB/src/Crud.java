import com.mindfusion.common.DateTime;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.model.*;

import java.sql.*;
import java.util.*;

final class Crud {


    static void save(Calendar calendar) {

        String name = calendar.getName();

        //save queries for new schedule
        String scheduleSQL = "INSERT INTO schedules (id, name) VALUES (?,?)";

        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement rStatement = null;
        try {

            connection = DriverManager.getConnection(Credentials.path,
                    Credentials.user, Credentials.password);

            if (!schedulesKeyMap.containsValue(name)) {
                schedulesKeyMap.put(schedulesKeyMap.isEmpty() ? 1 : schedulesKeyMap.lastKey() + 1, name);
                schedulesValueMap.put(name, schedulesKeyMap.lastKey());
                statement = connection.prepareStatement(scheduleSQL);
                statement.setInt(1, schedulesKeyMap.lastKey());
                statement.setString(2, name);
                statement.execute();
            }

            ResourceList<Contact> contactResourceList = calendar.getContacts();
            for (Contact contact : contactResourceList) {
                if (!savedContactsMap.containsKey(contact.getId())) {
                    insertContact(contact, name);
                }
            }

            ItemList appList = calendar.getSchedule().getItems();

            for (Item item : appList) {

                if (item instanceof Appointment) {

                    Appointment appointment = (Appointment) item;
                    if (appointment.getTag() == null && !savedAppMap.containsKey(appointment.getId()))
                        insertAppointment(appointment, name);
                    else if (updatedAppMap.containsKey(appointment.getId()))
                        updateAppointment(appointment);
                }
            }
            for (Appointment appointment: deletedAppMap.values())
                deleteAppointment(appointment);

            updatedAppMap.clear();
            deletedAppMap.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void insertContact(Contact contact, String calendarName) throws SQLException{

        String contactSQL = "INSERT INTO contacts (id, scheduleId, firstName, lastName, phoneNumber, emailAddress) VALUES (?,?,?,?,?,?)";
        String schedule_contact = "INSERT INTO schedule_contacts (scheduleId, contactId) VALUES (?,?)";


        Connection connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
        PreparedStatement statement = connection.prepareStatement(contactSQL);;
        PreparedStatement rStatement = connection.prepareStatement(schedule_contact);;

        statement.setString(1, contact.getId());
        statement.setInt(2, schedulesValueMap.get(calendarName));
        statement.setString(3, contact.getFirstName());
        statement.setString(4, contact.getLastName());
        statement.setString(5, contact.getPhone());
        statement.setString(6, contact.getEmail());
        statement.execute();
        rStatement.setInt(1, schedulesValueMap.get(calendarName));
        rStatement.setString(2, contact.getId());
        rStatement.execute();
        savedContactsMap.put(contact.getId(), contact);

        connection.close();
        statement.close();
        rStatement.close();
    }

    private static void insertAppointment(Appointment appointment,  String calendarName) throws SQLException {

        String appSQL = "INSERT INTO appointments (id, scheduleId, startTime, endTime, headerText, detailsText) VALUES (?,?,?,?,?,?)";
        String app_contact = "INSERT INTO appointments_contacts (scheduleId, appId, contactId) VALUES (?,?,?)";

        Connection connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
        PreparedStatement statement = connection.prepareStatement(appSQL);
        PreparedStatement rStatement = connection.prepareStatement(app_contact);

        appointment.setTag(schedulesValueMap.get(calendarName));

        statement.setString(1, appointment.getId());
        statement.setInt(2, schedulesValueMap.get(calendarName));
        statement.setString(3, String.valueOf(appointment.getStartTime().getTicks()));
        statement.setString(4, String.valueOf(appointment.getEndTime().getTicks()));
        statement.setString(5, appointment.getHeaderText());
        statement.setString(6, appointment.getDetails());
        statement.execute();
        for (Contact contact : appointment.getContacts()) {
            rStatement.setInt(1, schedulesValueMap.get(calendarName));
            rStatement.setString(2, appointment.getId());
            rStatement.setString(3, contact.getId());
            rStatement.execute();
        }
        appContactMap.put(appointment.getId(), new HashSet<>(appointment.getContacts()));
        savedAppMap.put(appointment.getId(), appointment);

        connection.close();
        statement.close();
        rStatement.close();
    }

    private static void updateAppointment(Appointment appointment) throws SQLException{

        String uQuery = "UPDATE appointments SET startTime = ?, endTime = ?, headerText= ?, detailsText = ?  WHERE  scheduleId = ? AND id = ?";
        String ucQuery = "UPDATE appointments_contacts SET  contactId = ? WHERE scheduleId = ? AND appId = ?";
        String app_contact = "INSERT INTO appointments_contacts (scheduleId, appId, contactId) VALUES (?,?,?)";
        String del_con = "DELETE FROM appointments_contacts WHERE appId = ? AND contactId = ? AND scheduleId = ?";

        Connection connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
        PreparedStatement statement = connection.prepareStatement(uQuery);
        PreparedStatement rStatement = connection.prepareStatement(ucQuery);
        PreparedStatement iStatement = connection.prepareStatement(app_contact);
        PreparedStatement dStatement = connection.prepareStatement(del_con);

        statement.setString(1, String.valueOf(appointment.getStartTime().getTicks()));
        statement.setString(2, String.valueOf(appointment.getEndTime().getTicks()));
        statement.setString(3, appointment.getHeaderText());
        statement.setString(4, appointment.getDetails());
        statement.setInt(5, (Integer) appointment.getTag());
        statement.setString(6, appointment.getId());
        statement.execute();
        for (Contact contact : appointment.getContacts()){

            if (appContactMap.get(appointment.getId()).size() == appointment.getContacts().size()){
                rStatement.setString(1, contact.getId());
                rStatement.setInt(2, (Integer) appointment.getTag());
                rStatement.setString(3, appointment.getId());
                rStatement.execute();
            }else if (appContactMap.get(appointment.getId()).size() < appointment.getContacts().size()
                    && !appContactMap.get(appointment.getId()).contains(contact)){
                iStatement.setInt(1, (Integer) appointment.getTag());
                iStatement.setString(2, appointment.getId());
                iStatement.setString(3, contact.getId());
                iStatement.execute();
            }else if (appContactMap.get(appointment.getId()).size() > appointment.getContacts().size() &&
                    appContactMap.get(appointment.getId()).removeAll(appointment.getContacts())){
                for (Contact c : appContactMap.get(appointment.getId())){
                    dStatement.setString(1, appointment.getId());
                    dStatement.setString(2, c.getId());
                    dStatement.setInt(3, (Integer) appointment.getTag());
                    dStatement.execute();
                }
                break;
            }
        }
        appContactMap.put(appointment.getId(), new HashSet<>(appointment.getContacts()));

        connection.close();
        statement.close();
        rStatement.close();
        iStatement.close();
        dStatement.close();
    }

    private static void deleteAppointment(Appointment appointment) throws SQLException {

        String dQuery = "DELETE appointments, appointments_contacts " +
                "FROM appointments " +
                "INNER JOIN appointments_contacts " +
                "ON appointments_contacts.appId = appointments.id " +
                "WHERE appointments.scheduleId = ? " +
                "AND appointments_contacts.scheduleId = ?" +
                " AND id = ? " +
                "AND appointments_contacts.appId = ?";

        Connection connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
        PreparedStatement statement = connection.prepareStatement(dQuery);

        statement.setInt(1, (Integer) appointment.getTag());
        statement.setInt(2, (Integer) appointment.getTag());
        statement.setString(3, appointment.getId());
        statement.setString(4, appointment.getId());
        statement.execute();

        connection.close();
        statement.close();

    }

    static void load(int selectedIndex, Calendar calendar) {


        String contactQuery = "SELECT id, firstName, lastName, phoneNumber, emailAddress FROM contacts WHERE contacts.scheduleId = " + selectedIndex + "";
        String appQuery = "SELECT id, startTime, endTime, headerText, detailsText, GROUP_CONCAT(DISTINCT contactId) " +
                "FROM appointments " +
                "LEFT JOIN appointments_contacts " +
                "ON appointments.id = appointments_contacts.appId " +
                "WHERE appointments.scheduleId = " + selectedIndex + "  AND appointments_contacts.scheduleId = "+selectedIndex+" GROUP BY id";

        Connection connection = null;
        PreparedStatement statement = null;
        appContactMap.clear();
        try {
            connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
            statement = connection.prepareStatement(contactQuery);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Contact contact = new Contact();
                contact.setId(resultSet.getString(1));
                contact.setFirstName(resultSet.getString(2));
                contact.setLastName(resultSet.getString(3));
                if (resultSet.getString(4) != null)
                    contact.setPhone(resultSet.getString(4));
                if (resultSet.getString(5) != null)
                    contact.setEmail(resultSet.getString(5));
                savedContactsMap.put(contact.getId(), contact);
                calendar.getSchedule().getContacts().add(contact);
                calendar.getContacts().add(contact);
            }
            statement = connection.prepareStatement(appQuery);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String appId = resultSet.getString(1);
                long startTime = Long.valueOf(resultSet.getString(2));
                long endTime = Long.valueOf(resultSet.getString(3));
                String h_text = resultSet.getString(4);
                String d_text = resultSet.getString(5);
                String[] c_ids = resultSet.getString(6).split(",");
                Appointment appointment = new Appointment();
                appointment.setId(appId);
                appointment.setTag(selectedIndex);
                appointment.setStartTime(new DateTime(startTime));
                appointment.setEndTime(new DateTime(endTime));
                appointment.setHeaderText(h_text);
                appointment.setDetails(d_text);
                savedAppMap.put(appId, appointment);
                for (String c_id : c_ids)
                    appointment.getContacts().add(savedContactsMap.get(c_id));
                appContactMap.put(appId, new HashSet<>(appointment.getContacts()));
                calendar.getSchedule().getItems().add(appointment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    static List<String> getSchedules() {

        String namesSQL = "SELECT id, name from schedules";
        Connection connection = null;
        PreparedStatement statement = null;
        schedulesKeyMap.clear();
        schedulesValueMap.clear();
        try {
            connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
            statement = connection.prepareStatement(namesSQL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                schedulesKeyMap.put(resultSet.getInt(1), resultSet.getString(2));
                schedulesValueMap.put(resultSet.getString(2), resultSet.getInt(1));
            }

        } catch (SQLException ignore) {

        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
        return new ArrayList<>(schedulesKeyMap.values());
    }

    static TreeMap<Integer, String> schedulesKeyMap = new TreeMap<>();
    static Map<String, Integer> schedulesValueMap = new Hashtable<>();
    static Map<String, Contact> savedContactsMap = new Hashtable<>();
    static Map<String, Appointment> savedAppMap = new Hashtable<>();
    static Map<String, Appointment> updatedAppMap = new Hashtable<>();
    static Map<String, Appointment> deletedAppMap = new Hashtable<>();
    private static  Map<String, Set<Contact>> appContactMap = new Hashtable<>();
}
