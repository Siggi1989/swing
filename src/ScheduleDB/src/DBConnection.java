import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.List;

class DBConnection {

    private String host = "jdbc:mysql://";
    private String database;
    private String userName;
    private String password;
    private static final String CONFIG = "?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC&characterEncoding=UTF-8";
    private String errorMsg = "";

    DBConnection(String host, String database, String userName, String passeord){

        this.host += host.endsWith("/") ? host.substring(0, host.length()-1) : host;
        this.database = database;
        this.userName = userName;
        this.password = passeord;

    }
    boolean connect(){

        Connection connection=null;
        Statement statement = null;
        boolean b;
        try {
            connection = DriverManager.getConnection(host + CONFIG, userName, password);
            ResultSet resultSet = connection.getMetaData().getCatalogs();
            boolean exists = false;
            while (resultSet.next()){
                String db = resultSet.getString(1);
                if (db.equals(database)){
                    exists = true;
                    break;
                }
            }
            if (!exists){
                statement = connection.createStatement();
                statement.executeUpdate("CREATE DATABASE " + database);
                Credentials.path = host+ "/" + database + CONFIG;
                Credentials.user = userName;
                Credentials.password = password;
                b = true;
            }else {
                Credentials.path = host+ "/" + database + CONFIG;
                Credentials.user = userName;
                Credentials.password = password;
                b = true;
            }
        } catch (SQLException e) {
            errorMsg = e.getMessage();
            b = false;
        }finally {
            if (connection != null){
                try{
                    connection.close();
                }catch (SQLException e){
                    errorMsg = e.getMessage();
                    b = false;
                }
            }
            if (statement != null){
                try {
                    statement.close();
                }catch (SQLException e){
                    errorMsg = e.getMessage();
                    b = false;
                }
            }
        }
        return b;
    }

    String getErrorMsg() {
         return errorMsg;
    }
    
    static void config() {
    	Connection connection = null;
		try
		{
			connection = DriverManager.getConnection(Credentials.path,Credentials.user,Credentials.password);
			CallableStatement st = connection.prepareCall("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,\'ONLY_FULL_GROUP_BY\',\'\'));");
			st.execute();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{

			try {
				if(connection != null) {
					connection.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

    }

    void createTables() {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            List<String> sqlQuery = Files.readAllLines(Paths.get(getClass().getResource("File/tables.sql").toURI()));
            connection = DriverManager.getConnection(Credentials.path, Credentials.user, Credentials.password);
            for (String sql : sqlQuery){

                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.executeUpdate();
            }



        }catch (SQLException | IOException | URISyntaxException e){
            e.printStackTrace();
        } finally {
            if (connection != null){

                try{
                    connection.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null){

                try{
                    preparedStatement.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
