/**
 * Copyright (c) 2022, MindFusion LLC - Bulgaria.
 */

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EnumSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.mindfusion.common.DateTime;
import com.mindfusion.common.DayOfWeek;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.CalendarAdapter;
import com.mindfusion.scheduling.CalendarView;
import com.mindfusion.scheduling.ItemMouseEvent;
import com.mindfusion.scheduling.model.*;
import com.mindfusion.scheduling.standardforms.AppointmentForm;
import com.mindfusion.scheduling.standardforms.DialogResult;


public class MainWindow extends JFrame
{
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					new MainWindow().setVisible(true);
				}
				catch (Exception exp)
				{
				}
			}
		});
	}

	protected MainWindow()
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 400);
		setTitle("Tutorial 3");

		calendar = new Calendar();
		getContentPane().add(calendar, BorderLayout.CENTER);

		calendar.beginInit();
		calendar.setAllowInplaceEdit(false);
		calendar.setCurrentView(CalendarView.WeekRange);
		calendar.setDate(new DateTime(DateTime.now().getYear()-1, 12, 1));
		calendar.setEndDate(new DateTime(DateTime.now().getYear(), 3, 1));
		calendar.setCurrentTime(new DateTime(DateTime.now().getYear(), 1, 10, 14, 0, 0));

		Appointment app = new Appointment();
		app.setHeaderText("Meet George");
		app.setDescriptionText("This is a sample appointment");
		app.setStartTime(new DateTime(DateTime.now().getYear(), 1, 10, 14, 0, 0));
		app.setEndTime(new DateTime(DateTime.now().getYear(), 1, 10, 16, 30, 0));

		calendar.getSchedule().getItems().add(app);

		Recurrence rec = new Recurrence();
		rec.setPattern(RecurrencePattern.Weekly);
		rec.setDaysOfWeek(
			EnumSet.of(DayOfWeek.Monday, DayOfWeek.Wednesday));
		rec.setWeeks(2);
		rec.setStartDate(new DateTime(DateTime.now().getYear()-1, 12, 1));
		rec.setRecurrenceEnd(RecurrenceEnd.Never);

		app.setRecurrence(rec);

		calendar.endInit();

		calendar.addCalendarListener(new CalendarAdapter()
		{
			@Override
			public void itemClick(ItemMouseEvent e)
			{
				// display the form if item is double-clicked
				if (e.getClicks() != 2)
					return;

				calendar.resetDrag();

				// create the form
				AppointmentForm form = new AppointmentForm(calendar.getSchedule());
				form.setTitle(e.getItem().getHeaderText());
				form.setAppointment((Appointment)e.getItem());
				form.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);

				// display the form to the user
				form.setVisible(true);
				form.addWindowListener(new WindowAdapter()
				{
					@Override
					public void windowClosed(WindowEvent we)
					{
						if (form.getDialogResult() == DialogResult.Remove)
						{
							// 'Remove' is returned when the user clicks the 'Delete' button.
							// It is up to us to delete the item from the schedule.
							if (e.getItem().getRecurrenceState() == RecurrenceState.Occurrence ||
								e.getItem().getRecurrenceState() == RecurrenceState.Exception)
								e.getItem().getRecurrence().markException(e.getItem(), true);
							else
								calendar.getSchedule().getItems().remove(e.getItem());
						}
					}
				});

				// repaint the control
				calendar.repaint();
			}
		});
	}


	private static final long serialVersionUID = 1L;

	private Calendar calendar;
}