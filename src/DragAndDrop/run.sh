cd `dirname $0`

export CLASSPATH=$CLASSPATH:./../../JPlanner.jar
mkdir ./bin
javac -d ./bin ./src/*.java
cd bin
mkdir ./Resources
cp ../Resources/*.* ./Resources/
java -cp ./:./../../../JPlanner.jar BaseWindow
cd ..
echo “Done”